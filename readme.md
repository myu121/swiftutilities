# Swift Utilities

This repository contains the following Swift utilities that can be dragged and dropped into any project for immediate use:

- File Manager
- Location Manager
- Email and Call
- Network Layer
- Image Picker

These are protocol based so that anyone can overwrite functionality for their own implementation.

## File Manager

This file manager provides an easy to use directory enum, the ability to read, write, rename, copy, move, and delete files.
It also provides file status checking and getting file meta data.

## Location Manager

This location manager provides one time location fetch, geocode, reverse geocode. Authorization is handled automatically.

## Email and Call

Simply create an object of the manager and easily use functions like mail and call in your project, or conform to the protocol
and add your own implementation.

## Network Layer



>class Api: EndPointType {
	>case yourEndPoint
>}

The network layer can be used to send an HTTP request to url and get back data. End points, base url, HTTP methods, HTTP tasks,
parameters and headers can be defined by conforming to the EndPointType protocol. Then, create a router instance with that new EndPointType
to use the request method.



>let router = Router<Api>()
>router.request(.yourEndPoint) { completion block }

## Image Picker

This image picker handles photo permissions and allows choosing image from photo library or camera roll.

## Technologies

iOS 13.0 or above

Xcode 12.5

Swift 5

## Supported Devices

iPhone SE (2nd gen)

iPhone 8 - 12 (All sizes supported)