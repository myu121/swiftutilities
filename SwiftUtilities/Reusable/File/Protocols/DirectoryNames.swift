//
//  AppDirectoryNames.swift
//  SwiftUtilities
//
//  Created by Michael Yu on 5/2/21.
//

import Foundation

// MARK: - Protocol
protocol AppDirectoryNames {
    func documentsDirectoryURL() -> URL
    
    func inboxDirectoryURL() -> URL
    
    func libraryDirectoryURL() -> URL
    
    func tempDirectoryURL() -> URL
    
    func getURL(for directory: AppDirectories) -> URL
    
    func buildFullPath(forFileName name: String, inDirectory directory: AppDirectories) -> URL
}

// MARK: - Extension
extension AppDirectoryNames {
    func documentsDirectoryURL() -> URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func inboxDirectoryURL() -> URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(AppDirectories.inbox.rawValue)
    }
    
    func libraryDirectoryURL() -> URL {
        if let url = FileManager.default.urls(for: FileManager.SearchPathDirectory.libraryDirectory, in: .userDomainMask).first {
            return url
        }
        return URL(fileURLWithPath: "")
    }
    
    func tempDirectoryURL() -> URL {
        FileManager.default.temporaryDirectory
    }
    
    func getURL(for directory: AppDirectories) -> URL {
        switch directory {
        case .documents:
            return documentsDirectoryURL()
            
        case .inbox:
            return inboxDirectoryURL()
            
        case .library:
            return libraryDirectoryURL()
            
        case .temp:
            return tempDirectoryURL()
        }
    }
    
    func buildFullPath(forFileName name: String, inDirectory directory: AppDirectories) -> URL {
        getURL(for: directory).appendingPathComponent(name)
    }
}
