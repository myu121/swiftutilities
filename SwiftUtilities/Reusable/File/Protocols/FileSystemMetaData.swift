//
//  AppFileSystemMetaData.swift
//  SwiftUtilities
//
//  Created by Michael Yu on 5/2/21.
//

import Foundation

// MARK: - Protocol
protocol AppFileSystemMetaData {
    func list(directory at: URL) -> Bool
    
    func attributes(ofFile atFullPath: URL) -> [FileAttributeKey: Any]
}
 
// MARK: - Extension
extension AppFileSystemMetaData {
    func list(directory at: URL) -> Bool {
        var listing: [String] = []
        do {
            listing = try FileManager.default.contentsOfDirectory(atPath: at.path)
        } catch {
        }
        
        if !listing.isEmpty {
            print("\n----------------------------")
            print("LISTING: \(at.path)")
            print("")
            for file in listing {
                print("File: \(file.debugDescription)")
            }
            print("")
            print("----------------------------\n")
            
            return true
        } else {
            return false
        }
    }
    
    func attributes(ofFile atFullPath: URL) -> [FileAttributeKey: Any] {
        var attributes: [FileAttributeKey: Any] = [:]
        do {
            attributes = try FileManager.default.attributesOfItem(atPath: atFullPath.path)
        } catch {
        }
        return attributes
    }
}
