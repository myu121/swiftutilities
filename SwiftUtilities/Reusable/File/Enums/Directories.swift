//
//  File.swift
//  SwiftUtilities
//
//  Created by Michael Yu on 5/2/21.
//

import Foundation

enum Directories: String {
    case documents = "Documents"
    case inbox = "Inbox"
    case library = "Library"
    case temp = "tmp"
}
