//
//  MediaAccessController.swift
//  FileManager
//
//  Created by Michael Yu on 4/20/21.
//

import UIKit
import MessageUI

class MediaAccess: NSObject, MediaAccessProtocol {
    // MARK: - Properties
    private var controller: UIViewController
    private var imagePicker: ImagePicker
    
    init(presentingController: UIViewController) {
        self.controller = presentingController
        super.init()
        imagePicker = ImagePicker(presentingController: self.controller, withDelegate: self)
    }
    
    // MARK: - Public Methods
    func openMedia() {
        self.accessMedia()
    }

    func accessMedia() {
        self.imagePicker.showOptions()
    }
    
    func mail(to recipients: [String] = [],
                      withAttachment attachment: (data: Data, mimeType: String, fileName: String) = (Data(), "", ""),
                      withSubject subject: String = "",
                      withMessage message: String = "") {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        let controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        
        controller.setToRecipients(recipients)
        if !attachment.fileName.isEmpty && !attachment.mimeType.isEmpty {
            controller.addAttachmentData(attachment.data, mimeType: attachment.mimeType, fileName: attachment.fileName)
        }
        if !subject.isEmpty {
            controller.setSubject(subject)
        }
        if !message.isEmpty {
            controller.setMessageBody(message, isHTML: false)
        }
        self.controller.present(controller, animated: true, completion: nil)

    }
    
    func message(to recipients: [String], withBody body: String) {
        guard MFMessageComposeViewController.canSendText() else {
            return
        }
        
        let controller = MFMessageComposeViewController()
        controller.messageComposeDelegate = self
        controller.recipients = recipients
        controller.body = body
    }
    
    func call(number: String) {
        guard let url = URL(string: "TEL://\(number)") else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
        }
    }
}

// MARK: - Extensions
extension MediaAccess: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension MediaAccess: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension MediaAccess: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
    }
    
    func didCancel() {
    }
}
