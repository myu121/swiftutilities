//
//  LocationService.swift
//  SwiftUtilities
//
//  Created by Michael Yu on 4/30/21.
//

import Foundation

protocol LocationService {
    // MARK: - Type Aliases
    
    typealias FetchLocationCompletion = (Location?, AppError?) -> Void

    // MARK: - PublicMethods
    
    func fetchLocation(completion: @escaping FetchLocationCompletion)
}
